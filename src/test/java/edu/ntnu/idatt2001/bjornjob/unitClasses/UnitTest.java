package edu.ntnu.idatt2001.bjornjob.unitClasses;

import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.InfantryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */

@DisplayName("Unit tests")
public class UnitTest {
    private InfantryUnit testUnitOne;
    private InfantryUnit testUnitTwo;

    @BeforeEach
    public void testData() {
        testUnitOne = new InfantryUnit("Bruce Banner",100,20,8);
        testUnitTwo = new InfantryUnit("Peter Parker",100,30,10);
    }

    @Nested
    @DisplayName("Positive tests for the superclass ´Unit´")
    public class PositiveTests {

        @Test
        @DisplayName("Testing invalid set-value for health")
        public void testSetHealthMethod() {
            testUnitOne.setHealth(60);
            assertEquals(60, testUnitOne.getHealth());
            testUnitOne.setHealth(-1);
            assertEquals(0, testUnitOne.getHealth());
        }

        @Test
        @DisplayName("Testing attack-method")
        public void testAttackMethod() {
            testUnitOne.attack(testUnitTwo, 1);

            assertTrue(testUnitTwo.getHealth() < 100);
        }

        @Nested
        @DisplayName("Tests for valid input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Valid name-input in constructor for the super class Unit")
            public void testValidInputForNameInConstructor() {
                assertEquals("Bruce Banner", testUnitOne.getName());
            }

            @Test
            @DisplayName("Valid health-input in constructor for the super class Unit")
            public void testValidInputForHealthInConstructor() {
                assertEquals(100, testUnitOne.getHealth());
            }

            @Test
            @DisplayName("Valid attack-input in constructor for the super class Unit")
            public void testValidInputForAttackInConstructor() {
                assertEquals(20, testUnitOne.getAttack());
            }

            @Test
            @DisplayName("Valid armor-input in constructor for the super class Unit")
            public void testValidInputForArmorInConstructor() {
                assertEquals(8, testUnitOne.getArmor());
            }
        }
    }

    @Nested
    @DisplayName("Negative tests for the superclass ´Unit´")
    public class NegativeTests {

        @Nested
        @DisplayName("Tests for incorrect input in the constructor")
        public class ConstructorTests {

            @Test
            @DisplayName("Invalid (null) input in constructor for the super class Unit")
            public void testNullInputForNameInConstructor() {
                try {
                    testUnitOne = new InfantryUnit(null,100,20,10);
                } catch (NullPointerException e) {
                    assertThrows(NullPointerException.class, () -> new InfantryUnit(null, 100,
                            20,8));
                    assertEquals("Name of the unit cannot be null!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid (empty string) input in constructor for the super class Unit")
            public void testEmptyStringInputForNameInConstructor() {
                try {
                    testUnitOne = new InfantryUnit("",100,20,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("",100,
                            20,10));
                    assertEquals("Name of the unit cannot be blank!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid health-input in constructor for the super class Unit")
            public void testInvalidHealthInputInConstructor() {
                try {
                    testUnitOne = new InfantryUnit("Bruce Banner",-1,20,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Bruce Banner",-1,
                            20,10));
                    assertEquals("Health of the unit cannot be negative or zero!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid attack-input in constructor for the super class Unit")
            public void testInvalidAttackInputInConstructor() {
                try {
                    testUnitOne = new InfantryUnit("Bruce Banner",100,-1,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Bruce Banner",100,
                            -1,10));
                    assertEquals("Attack for the unit cannot be negative!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid armor-input in constructor for the super class Unit")
            public void testInvalidArmorInputInConstructor() {
                try {
                    testUnitOne = new InfantryUnit("Bruce Banner",100,20,-1);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Bruce Banner",100,
                            20,-1));
                    assertEquals("Armor for the unit cannot be negative!", e.getMessage());
                }
            }
        }
    }
}
