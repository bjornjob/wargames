package edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
@DisplayName("Ranged unit tests")
public class RangedUnitTest {
    private RangedUnit testUnitOne;
    private RangedUnit testUnitTwo;

    @BeforeEach
    public void testData() {
        testUnitOne = new RangedUnit("Bruce Banner",100,20,8);
        testUnitTwo = new RangedUnit("Peter Parker",100,30,10);
    }

    @Nested
    @DisplayName("Positive tests for the subclass ´RangedUnit´")
    public class PositiveTests {

        @Test
        @DisplayName("Testing both valid and invalid set-value for health")
        public void testSetHealthMethod() {
            testUnitOne.setHealth(60);
            assertEquals(60, testUnitOne.getHealth());
            testUnitOne.setHealth(-1);
            assertEquals(0, testUnitOne.getHealth());
        }

        @Test
        @DisplayName("Testing attack-method")
        public void testAttackMethod() {
            testUnitOne.attack(testUnitTwo, 1);
            assertTrue(testUnitTwo.getHealth() < 100);
        }

        @Test
        @DisplayName("Testing the method ´getAttackBonus´")
        public void testAttackBonus() {
            assertEquals(5, testUnitOne.getAttackBonus(1));
            assertEquals(3, testUnitOne.getAttackBonus(2));
            assertEquals(2, testUnitOne.getAttackBonus(3));
        }

        @Test
        @DisplayName("Testing method ´getResistBonus´")
        public void testResistBonus() {
            testUnitOne.attack(testUnitTwo, 1);
            assertEquals(6, testUnitOne.getResistBonus(1));
            testUnitOne.attack(testUnitTwo, 1);
            assertEquals(4, testUnitOne.getResistBonus(1));
            testUnitOne.attack(testUnitTwo, 1);
            assertEquals(2, testUnitOne.getResistBonus(1));
        }

        @Nested
        @DisplayName("Tests for valid input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Valid name-input in constructor for the subclass 'RangedUnit'")
            public void testValidInputForNameInConstructor() {
                assertEquals("Bruce Banner", testUnitOne.getName());
            }

            @Test
            @DisplayName("Valid health-input in constructor for the subclass 'RangedUnit'")
            public void testValidInputForHealthInConstructor() {
                assertEquals(100, testUnitOne.getHealth());
            }

            @Test
            @DisplayName("Valid attack-input in constructor for the subclass 'RangedUnit'")
            public void testValidInputForAttackInConstructor() {
                assertEquals(20, testUnitOne.getAttack());
            }

            @Test
            @DisplayName("Valid armor-input in constructor for subclass 'RangedUnit'")
            public void testValidInputForArmorInConstructor() {
                assertEquals(8, testUnitOne.getArmor());
            }
        }
    }

    @Nested
    @DisplayName("Negative tests for the subclass 'RangedUnit'")
    public class NegativeTests {

        @Nested
        @DisplayName("Tests for incorrect input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Invalid (null) input in constructor for the subclass 'RangedUnit'")
            public void testNullInputForNameInConstructor() {
                try {
                    testUnitOne = new RangedUnit(null,100,20,10);
                } catch (NullPointerException e) {
                    assertThrows(NullPointerException.class, () -> new RangedUnit(null, 100,
                            20,8));
                    assertEquals("Name of the unit cannot be null!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid (empty string) input in constructor for the subclass 'RangedUnit'")
            public void testEmptyStringInputForNameInConstructor() {
                try {
                    testUnitOne = new RangedUnit("",100,20,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new RangedUnit("", 100,
                            20,8));
                    assertEquals("Name of the unit cannot be blank!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid health-input in constructor for the subclass 'RangedUnit'")
            public void testInvalidHealthInputInConstructor() {
                try {
                    testUnitOne = new RangedUnit("Bruce Banner",-1,20,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new RangedUnit("Bruce Banner", -1,
                            20,8));
                    assertEquals("Health of the unit cannot be negative or zero!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid attack-input in constructor for the subclass 'RangedUnit'")
            public void testInvalidAttackInputInConstructor() {
                try {
                    testUnitOne = new RangedUnit("Bruce Banner",100,-1,10);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new RangedUnit("Bruce Banner", 100,
                            -1,8));
                    assertEquals("Attack for the unit cannot be negative!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid armor-input in constructor for the subclass 'RangedUnit'")
            public void testInvalidArmorInputInConstructor() {
                try {
                    testUnitOne = new RangedUnit("Bruce Banner",100,20,-1);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new RangedUnit("Bruce Banner", 100,
                            20,-1));
                    assertEquals("Armor for the unit cannot be negative!", e.getMessage());
                }
            }
        }
    }
}