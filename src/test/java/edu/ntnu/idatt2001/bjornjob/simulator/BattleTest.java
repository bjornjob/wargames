package edu.ntnu.idatt2001.bjornjob.simulator;

import edu.ntnu.idatt2001.bjornjob.utilities.UnitFactory;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.TerrainType;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.UnitType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
@DisplayName("Battle tests")
public class BattleTest {
    private Army humanArmy;
    private Army orchishArmy;

    @BeforeEach
    public void TestData() {
        humanArmy = new Army("Human army");
        orchishArmy = new Army("Orchish army");

        humanArmy.add(UnitFactory.generateUnit(UnitType.INFANTRY_UNIT, "Footman", 100));
        humanArmy.add(UnitFactory.generateUnit(UnitType.CAVALRY_UNIT, "Knight", 100));
        humanArmy.add(UnitFactory.generateUnit(UnitType.RANGED_UNIT, "Archer", 100));
        humanArmy.add(UnitFactory.generateUnit(UnitType.COMMANDER_UNIT, "Mountain King", 180));

        orchishArmy.add(UnitFactory.generateUnit(UnitType.INFANTRY_UNIT, "Grunt", 100));
    }

    @Nested
    @DisplayName("Positive tests for the class ´Army´")
    public class PositiveTests {

        @Test
        @DisplayName("Human army wins when they are superior in numbers")
        public void testThatHumanArmyWinsWhenSuperiorInNumbers() {
            Battle testBattle = new Battle(humanArmy, orchishArmy, TerrainType.HILL);
            testBattle.simulate();
            assertFalse(orchishArmy.hasUnits());
            assertTrue(humanArmy.hasUnits());
        }

        @Test
        @DisplayName("Orcish wins when they are superior in numbers")
        public void testThatOrcishArmyWinsWhenSuperiorInNumbers() {
            orchishArmy.add(UnitFactory.generateUnit(UnitType.CAVALRY_UNIT, "Raider", 100));
            orchishArmy.add(UnitFactory.generateUnit(UnitType.CAVALRY_UNIT, "Raider", 100));
            orchishArmy.add(UnitFactory.generateUnit(UnitType.RANGED_UNIT, "Spearman", 100));
            orchishArmy.add(UnitFactory.generateUnit(UnitType.RANGED_UNIT, "Spearman", 100));
            orchishArmy.add(UnitFactory.generateUnit(UnitType.COMMANDER_UNIT, "Gul´dan", 180));
            orchishArmy.add(UnitFactory.generateUnit(UnitType.COMMANDER_UNIT, "Gul´dan", 180));

            Battle testBattle = new Battle(humanArmy, orchishArmy, TerrainType.HILL);
            testBattle.simulate();
            assertFalse(humanArmy.hasUnits());
            assertTrue(orchishArmy.hasUnits());
        }

        @Nested
        @DisplayName("Tests for valid input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Valid object-input in constructor")
            public void testValidInputForArmyObjects() {
                try {
                    Battle testBattleOne = new Battle(humanArmy, orchishArmy, TerrainType.HILL);
                    Battle testBattleTwo = new Battle(humanArmy, orchishArmy, TerrainType.PLAINS);
                    Battle testBattleThree = new Battle(humanArmy, orchishArmy, TerrainType.FOREST);
                } catch (Exception e) {
                    fail("'testValidInputForArmyObjects' failed");
                }
            }
        }
    }

    @Nested
    @DisplayName("Negative tests for the class ´Army´")
    public class NegativeTests {

        @Nested
        @DisplayName("Tests for incorrect input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Try adding empty armies to a battle")
            public void testAddingEmptyArmiesToABattle() {
                Army emptyHumanArmy = new Army("Human army");
                Army emptyOrcishArmy = new Army("Orcish army");
                try {
                    Battle testBattleOne = new Battle(emptyHumanArmy, emptyOrcishArmy, TerrainType.HILL);
                    Battle testBattleTwo = new Battle(emptyHumanArmy, humanArmy, TerrainType.HILL);
                    Battle testBattleThree = new Battle(orchishArmy, emptyOrcishArmy, TerrainType.HILL);
                    Battle testBattleFour = new Battle(orchishArmy, emptyOrcishArmy, TerrainType.HILL);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () ->
                            new Battle(emptyHumanArmy, emptyOrcishArmy, TerrainType.HILL));
                    assertThrows(IllegalArgumentException.class, () ->
                            new Battle(emptyHumanArmy, emptyOrcishArmy, TerrainType.HILL));
                    assertThrows(IllegalArgumentException.class, () ->
                            new Battle(emptyHumanArmy, emptyOrcishArmy, TerrainType.HILL));
                    assertEquals("Inputted army must contain soldiers!", e.getMessage());
                }
            }
        }
    }
}