package edu.ntnu.idatt2001.bjornjob.simulator;

import edu.ntnu.idatt2001.bjornjob.unitClasses.*;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
@DisplayName("Army tests")
public class ArmyTest {
    private Army humanArmy;

    @BeforeEach
    public void TestData() {
        InfantryUnit newHumanUnitOne = new InfantryUnit("Footman", 100);
        CavalryUnit newHumanUnitTwo = new CavalryUnit("Knight", 100);
        RangedUnit newHumanUnitThree = new RangedUnit("Archer", 100);
        CommanderUnit newHumanUnitFour = new CommanderUnit("Mountain King", 180);

        humanArmy = new Army("Human army");

        humanArmy.add(newHumanUnitOne);
        humanArmy.add(newHumanUnitTwo);
        humanArmy.add(newHumanUnitThree);
        humanArmy.add(newHumanUnitFour);
    }

    @Nested
    @DisplayName("Positive tests for the class ´Army´")
    public class PositiveTests {

        @Test
        @DisplayName("Adding a unit to an army")
        public void testAddingASingleUnitToAnArmy() {
            InfantryUnit testUnit = new InfantryUnit("Footman", 100);

            boolean foundNewUnit = humanArmy.getAllUnits().contains(testUnit);
            assertFalse(foundNewUnit);

            humanArmy.add(testUnit);

            foundNewUnit = humanArmy.getAllUnits().contains(testUnit);
            assertTrue(foundNewUnit);
        }

        @Test
        @DisplayName("Adding several units to an army")
        public void testAddingSeveralUnitsToAnArmy() {
            ArrayList<Unit> testList = new ArrayList<>();
            InfantryUnit testUnitOne = new InfantryUnit("Footman", 100);
            CavalryUnit testUnitTwo = new CavalryUnit("Knight", 100);
            testList.add(testUnitOne);
            testList.add(testUnitTwo);

            assertFalse(humanArmy.getAllUnits().contains(testUnitOne));
            assertFalse(humanArmy.getAllUnits().contains(testUnitOne));

            humanArmy.addAll(testList);

            assertTrue(humanArmy.getAllUnits().contains(testUnitOne));
            assertTrue(humanArmy.getAllUnits().contains(testUnitOne));
        }

        @Test
        @DisplayName("Removing a unit from an army")
        public void testRemovingAUnitFromAnArmy() {
            RangedUnit testUnit = new RangedUnit("Spearman", 100);

            humanArmy.add(testUnit);
            assertTrue(humanArmy.getAllUnits().contains(testUnit));

            humanArmy.remove(testUnit);
            assertFalse(humanArmy.getAllUnits().contains(testUnit));
        }

        @Test
        @DisplayName("Checking if an army has any units")
        public void testWetherAnArmyContainsAnyUnits() {
            Army orcishArmy = new Army("Orcish Army");
            assertFalse(orcishArmy.hasUnits());

            assertTrue(humanArmy.hasUnits());
        }

        @Test
        @DisplayName("The method 'getInfantryUnits' returns the correct type and amount of units")
        public void testGetMethodForListOfInfantryUnits() {
            assertEquals(1, humanArmy.getInfantryUnits().size());
            assertTrue(humanArmy.getInfantryUnits()
                    .stream()
                    .allMatch(unit -> unit instanceof InfantryUnit));
        }

        @Test
        @DisplayName("The method 'getCavalryUnits' returns the correct type and amount of units")
        public void testGetMethodForListOfCavalryUnits() {
            assertEquals(1, humanArmy.getCavalryUnits().size());
            assertTrue(humanArmy.getCavalryUnits()
                    .stream()
                    .allMatch(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit)));
        }

        @Test
        @DisplayName("The method 'getRangedUnits' returns the correct type and amount of units")
        public void testGetMethodForListOfRangedUnits() {
            assertEquals(1, humanArmy.getRangedUnits().size());
            assertTrue(humanArmy.getRangedUnits()
                    .stream()
                    .allMatch(unit -> unit instanceof RangedUnit));
        }

        @Test
        @DisplayName("The method 'getCommanderUnits' returns the correct type and amount of units")
        public void testGetMethodForListOfCommanderUnits() {
            assertEquals(1, humanArmy.getCommanderUnits().size());
            assertTrue(humanArmy.getCommanderUnits()
                    .stream()
                    .allMatch(unit -> unit instanceof CommanderUnit));
        }

        @Nested
        @DisplayName("Tests for valid input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Valid name-input in constructor")
            public void testValidInputForNameInConstructor() {
                try {
                    Army testArmy = new Army("The E street band");
                } catch (Exception e) {
                    fail("'testValidInputForNameInConstructor' failed");
                }
            }

            @Test
            @DisplayName("Valid name-input and list-type in constructor")
            public void testValidInputForNameAndListInConstructor() {
                try {
                    ArrayList<Unit> unitsTestArray = new ArrayList<>();
                    unitsTestArray.add(new CavalryUnit("Bruce Springsteen", 75, 12, 10));
                    unitsTestArray.add(new CavalryUnit("Little Steven", 65, 10, 10));
                    Army testArmy = new Army("The E street band", unitsTestArray);
                } catch (Exception e) {
                    fail("'testValidInputForNameAndListInConstructor' failed");
                }
            }
        }
    }

    @Nested
    @DisplayName("Negative tests for the class ´Army´")
    public class NegativeTests {

        @Nested
        @DisplayName("Tests for incorrect input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Invalid name-input (null) in the constructor for the class `Army`")
            public void testNullInputForNameInConstructor() {
                try {
                    Army army = new Army(null);
                } catch (NullPointerException e) {
                    assertThrows(NullPointerException.class, () -> new Army(null));
                    assertEquals("The name for an army cannot be null, please try again!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid name-input (empty string) in the constructor for the class `Army`")
            public void testEmptyStringInputForNameInConstructor() {
                try {
                    Army army = new Army("");
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new Army(""));
                    assertEquals("The name for an army cannot be blank, please try again!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Invalid inputted list type in the constructor for the class `Army`")
            public void testInvalidListTypeInputInConstructor() {
                LinkedList<Unit> testList = new LinkedList<>();
                try {
                    Army testArmy = new Army("Army", testList);
                } catch (IllegalArgumentException e) {
                    assertThrows(IllegalArgumentException.class, () -> new Army("Army", testList));
                    assertEquals("The list-type must be an arraylist, please try again!", e.getMessage());
                }
            }

            @Test
            @DisplayName("Try to remove non-existing unit from an army")
            public void testRemovingUnitWhenUnitDoesNotExistInArmy() {
                Army testArmy = new Army("Test Army");
                InfantryUnit testUnit = new InfantryUnit("Footman", 100);
                try {
                    testArmy.remove(testUnit);
                } catch (IllegalArgumentException e) {
                    assertEquals("The unit does not exist!", e.getMessage());
                }
            }
        }
    }
}