package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.CavalryUnit;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.CommanderUnit;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.InfantryUnit;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.RangedUnit;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.UnitType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
@DisplayName("UnitFactory tests")
public class UnitFactoryTest {

    @Nested
    @DisplayName("Positive tests for 'UnitFactory'")
    public class PositiveTests {

        @Test
        @DisplayName("The method 'generateUnit' generates a unit with requested variables")
        public void testUnitGeneration() {
            Unit testUnitOne = UnitFactory.generateUnit(UnitType.INFANTRY_UNIT, "Human", 100);
            assertInstanceOf(InfantryUnit.class, testUnitOne);
            assertEquals("Human", testUnitOne.getName());
            assertEquals(100, testUnitOne.getHealth());

            Unit testUnitTwo = UnitFactory.generateUnit(UnitType.RANGED_UNIT, "Human", 100);
            assertInstanceOf(RangedUnit.class, testUnitTwo);

            Unit testUnitThree = UnitFactory.generateUnit(UnitType.CAVALRY_UNIT, "Human", 100);
            assertInstanceOf(CavalryUnit.class, testUnitThree);

            Unit testUnitFour = UnitFactory.generateUnit(UnitType.COMMANDER_UNIT, "Human", 100);
            assertInstanceOf(CommanderUnit.class, testUnitFour);
        }

        @Test
        @DisplayName("The method 'generateListOfUnits' generates units with requested variables")
        public void testListOfUnitsGeneration() {
            List<Unit> testUniListtOne = UnitFactory.generateListOfUnits
                    (UnitType.INFANTRY_UNIT, "Human", 100, 5);
            assertEquals(5, testUniListtOne.size());

            List<Unit> testUnitListTwo = UnitFactory.generateListOfUnits
                    (UnitType.INFANTRY_UNIT, "Human", 100, 5);
            assertEquals(5, testUnitListTwo.size());

            List<Unit> testUnitListThree = UnitFactory.generateListOfUnits
                    (UnitType.INFANTRY_UNIT, "Human", 100, 5);
            assertEquals(5, testUnitListThree.size());
        }
    }

    @Nested
    @DisplayName("Negative tests for 'UnitFactory'")
    public class NegativeTests {

        @Test
        @DisplayName("Invalid number of units requested in the method 'generateListOfUnits'")
        public void testUnitGeneration() {
            try {
                UnitFactory.generateListOfUnits
                        (UnitType.INFANTRY_UNIT, "Human", 100, -1);
                UnitFactory.generateListOfUnits
                        (UnitType.RANGED_UNIT, "Human", 100, -1);
                UnitFactory.generateListOfUnits
                        (UnitType.CAVALRY_UNIT, "Human", 100, -1);
                UnitFactory.generateListOfUnits
                        (UnitType.COMMANDER_UNIT, "Human", 100, -1);
            } catch (IllegalArgumentException e) {
                assertThrows(IllegalArgumentException.class, () -> UnitFactory.generateListOfUnits
                        (UnitType.INFANTRY_UNIT, "Human", 100, -1));
                assertEquals("You cannot create a negative amount of units!", e.getMessage());
            }
        }
    }
}