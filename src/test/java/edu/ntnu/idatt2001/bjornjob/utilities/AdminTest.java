package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.utilities.enums.TerrainType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AdminTest {

    @DisplayName("Negative test for the class ´AdminTest´")
    public static class NegativeTests {

        @Test
        @DisplayName("Null-input for an army in method 'setBattle'")
        public void testNullInputForArmiesInMethodSetBatle() {
            try {
                Admin.setBattle(TerrainType.HILL);
            } catch (NullPointerException e) {
                assertThrows(NullPointerException.class, () -> Admin.setBattle(TerrainType.HILL));
                assertEquals("One of the inputted armies are null!", e.getMessage());
            }
        }
    }
}
