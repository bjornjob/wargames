package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.simulator.Army;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.UnitType;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
public class FileHandlingTest {
    private Army testArmy;
    private FileHandling fileHandling;

    @BeforeEach
    public void testData() {
        fileHandling = new FileHandling();
        testArmy = new Army("Test Army");

        testArmy.addAll(UnitFactory.generateListOfUnits(UnitType.INFANTRY_UNIT, "Grunt", 100,
                20));
        testArmy.addAll(UnitFactory.generateListOfUnits(UnitType.RANGED_UNIT, "Spearman", 100,
                16));
        testArmy.addAll(UnitFactory.generateListOfUnits(UnitType.CAVALRY_UNIT, "Raider", 100,
                8));
        testArmy.addAll(UnitFactory.generateListOfUnits(UnitType.COMMANDER_UNIT, "Gul´dan", 100,
                2));

    }

    @Nested
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    @DisplayName("Positive tests for the class ´FileHandling´")
    public class PositiveTests {

        @Test
        @Order(1)
        @DisplayName("Testing that the creation of new files are possible")
        public void testWritingAnArmyToCsv() {
            try {
                fileHandling.writeArmy(testArmy, new File("src/test/resources/Army.csv"));
            } catch (IOException e) {
                fail(e.getMessage());
            }
        }

        @Test
        @Order(2)
        @DisplayName("Testing that the reading of existing files are possible and that the data is read correctly")
        public void testReadingAnArmyFromCsv() {
            Army army = null;
            try {
                army = fileHandling.readArmy(new File("src/test/resources/Army.csv"));
            } catch (IOException e) {
                fail(e.getMessage());
            }
            assertEquals(46, army.getAllUnits().size());
            assertEquals(20, army.getInfantryUnits().size());
            assertEquals(16, army.getRangedUnits().size());
            assertEquals(8, army.getCavalryUnits().size());
            assertEquals(2, army.getCommanderUnits().size());
        }
    }

    @Nested
    @DisplayName("Negative tests for the class ´FileHandling´")
    public class NegativeTests {

        @Test
        @DisplayName("Testing that writing an army to a .txt file fails")
        public void testWritingAnArmyToTxt() {
            try {
                fileHandling.writeArmy(testArmy, new File("src/test/resources/ArmyTextFile.txt"));
            } catch (IOException e) {
                assertThrows(IOException .class, () ->
                        fileHandling.writeArmy(testArmy, new File("src/test/resources/ArmyTextFile.txt")));
                assertEquals("Unsupported file format. Only .csv-files are supported.", e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading an army from a .txt file fails")
        public void testReadingAnArmyFromTxt() {
            try {
                Army army = fileHandling.readArmy(new File("src/test/resources/Army.txt"));
            } catch (IOException e) {
                assertThrows(IOException .class, () ->
                        fileHandling.readArmy(new File("src/test/resources/ArmyTextFile.txt")));
                assertEquals("Unsupported file format. Only .csv-files are supported.", e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading a file with the incorrect length fails")
        public void testReadingAFileWithWrongLength() {
            try {
                Army army = fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithIncorrectLength.csv"));
            } catch (IOException e) {
                assertThrows(IOException .class, () -> fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithIncorrectLength.csv")));
                assertEquals("Unable to read unit data from the file 'ArmyFileWithIncorrectLength.csv': " +
                        "Line data 'InfantryUnit,Footman,' is invalid. Make sure each line is correctly formatted.",
                        e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading a file with a non-integer value as health value fails")
        public void testReadingAFileWithNonIntegerValueForHealth() {
            try {
                Army army = fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNonIntegerHealthValue.csv"));
            } catch (IOException e) {
                assertThrows(IOException .class, () -> fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNonIntegerHealthValue.csv")));
                assertEquals("Unable to read unit data from the file 'ArmyFileWithNonIntegerHealthValue.csv': " +
                        "Health value must be an integer (For input string: \"#\").", e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading a file with zero for health value fails")
        public void testReadingAFileWithZeroValueForHealth() {
            try {
                Army army = fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithZeroHealthValue.csv"));
            } catch (IOException e) {
                assertThrows(IOException .class, () -> fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNonIntegerHealthValue.csv")));
                assertEquals("Unable to read unit data from the file 'ArmyFileWithZeroHealthValue.csv': " +
                        "Health value must be a positive integer.", e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading a file with a zero or negative value as health value fails")
        public void testReadingAFileWithNegativeValueForHealth() {
            try {
                Army army = fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNegativeHealthValue.csv"));
            } catch (IOException e) {
                assertThrows(IOException .class, () -> fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNonIntegerHealthValue.csv")));
                assertEquals("Unable to read unit data from the file 'ArmyFileWithNegativeHealthValue.csv': " +
                        "Health value must be a positive integer.", e.getMessage());
            }
        }

        @Test
        @DisplayName("Testing that reading a file with a zero or negative value as health value fails")
        public void testReadingAFileWithInvalidUnitTypeValueForHealth() {
            try {
                Army army = fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithInvalidUnitType.csv"));
            } catch (IOException e) {
                assertThrows(IOException .class, () -> fileHandling
                        .readArmy(new File("src/test/resources/ArmyFileWithNonIntegerHealthValue.csv")));
                assertEquals("Unable to read unit data from the file 'ArmyFileWithInvalidUnitType.csv': " +
                        "The unit type: 'BatMan' does not exist.", e.getMessage());
            }
        }
    }
}
