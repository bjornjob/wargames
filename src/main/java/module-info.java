module edu.ntnu.idatt2001.bjornjob.wargames {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.bjornjob to javafx.fxml;
    exports edu.ntnu.idatt2001.bjornjob;
    exports edu.ntnu.idatt2001.bjornjob.simulator;
    exports edu.ntnu.idatt2001.bjornjob.unitClasses;
    exports edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;
    exports edu.ntnu.idatt2001.bjornjob.utilities;
    exports edu.ntnu.idatt2001.bjornjob.utilities.enums;
    opens edu.ntnu.idatt2001.bjornjob.simulator to javafx.fxml;
    opens edu.ntnu.idatt2001.bjornjob.unitClasses to javafx.fxml;
    opens edu.ntnu.idatt2001.bjornjob.unitClasses.specialized to javafx.fxml;
    opens edu.ntnu.idatt2001.bjornjob.utilities to javafx.fxml;
    opens edu.ntnu.idatt2001.bjornjob.utilities.enums to javafx.fxml;
    exports edu.ntnu.idatt2001.bjornjob.guiControllers;
    opens edu.ntnu.idatt2001.bjornjob.guiControllers to javafx.fxml;
}