package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.simulator.Army;
import edu.ntnu.idatt2001.bjornjob.simulator.Battle;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.TerrainType;

public class Admin {
    private static Army armyOne;
    private static Army armyTwo;
    private static Battle battle;

    private Admin() {}

    public static Army getArmyOne() {
        return armyOne;
    }

    public static Army getArmyTwo() {
        return armyTwo;
    }

    public static Battle getBattle() {
        return battle;
    }

    public static void setArmyOne(Army armyOne) {
        Admin.armyOne = armyOne;
    }

    public static void setArmyTwo(Army armyTwo) {
        Admin.armyTwo = armyTwo;
    }

    public static void setBattle(TerrainType terrainType) throws NullPointerException, IllegalArgumentException {
        if (armyOne == null || armyTwo == null) {
            throw new NullPointerException("One of the inputted armies are null!");
        }
        Admin.battle = new Battle(armyOne, armyTwo, terrainType);
    }
}
