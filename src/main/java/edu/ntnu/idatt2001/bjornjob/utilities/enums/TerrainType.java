package edu.ntnu.idatt2001.bjornjob.utilities.enums;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link TerrainType} is an enum class used to represent the terrain type of which the armies battle on. Each enum is
 * represented by an integer.
 */
public enum TerrainType {
    HILL(1),
    PLAINS(2),
    FOREST(3);

    private final int code;

    /**
     * A constructor which appoints an integer value to each of the enums.
     * @param code is an integer value (1-3) representing each enum.
     */
    TerrainType(int code) {
        this.code = code;
    }

    /**
     * Get method for the code inputted in the constructor.
     * @return the integer value representing each enum.
     */
    public int getCode() {
        return this.code;
    }
}