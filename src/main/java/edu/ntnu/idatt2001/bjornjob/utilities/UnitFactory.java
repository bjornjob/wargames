package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.*;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.UnitType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link UnitFactory} is a class implementing Factory Design Pattern in order to have the option of instantiating
 * different types of units based on java enums.
 */
public class UnitFactory {

    /**
     * Private constructor for the class {@link UnitFactory}. Private in order make in impossible to instantiate any
     * objects of the class as the class methods are static.
     */
    private UnitFactory() {}

    /**
     * A method which constructs a single unit based on unit type, name and health.
     * @param unitType is an enum representing the subclass of {@link Unit} which the method should create an object of.
     * @param unitName is a string representing the name of the unit. This cannot be null or blank.
     * @param unitHealth is an integer representing the health of the unit. This cannot be zero or negative.
     * @return an object of the specified subclass of {@link Unit} with specified values for name and health.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public static Unit generateUnit(UnitType unitType, String unitName, int unitHealth) throws IllegalArgumentException,
            NullPointerException {
        return switch (unitType) {
            case INFANTRY_UNIT -> new InfantryUnit(unitName, unitHealth);
            case RANGED_UNIT -> new RangedUnit(unitName, unitHealth);
            case CAVALRY_UNIT -> new CavalryUnit(unitName, unitHealth);
            case COMMANDER_UNIT -> new CommanderUnit(unitName, unitHealth);
        };
    }

    /**
     * A method which constructs a list of units based on unit type, name and health. The number of units instantiated
     * are given by an inputted integer.
     * These units are created utilizing the method {@link #generateUnit(UnitType, String, int)} of this class.
     * @param unitType is an enum representing the subclass of {@link Unit} which the method should create an object of.
     * @param unitName is a string representing the name of the unit. This cannot be null or blank.
     * @param unitHealth is an integer representing the health of the unit. This cannot be zero or negative.
     * @return a list containing objects of the specified subclass of {@link Unit} with specified values for name and
     * health.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public static List<Unit> generateListOfUnits(UnitType unitType, String unitName, int unitHealth,
                                                 int numberOfUnitsRequested)
            throws IllegalArgumentException, NullPointerException {
        if (numberOfUnitsRequested < 0) {
            throw new IllegalArgumentException("You cannot create a negative amount of units!");
        }

        List<Unit> units = new ArrayList<>();

        for(int i = 0; i < numberOfUnitsRequested; i++) {
            units.add(generateUnit(unitType, unitName, unitHealth));
        }

        return units;
    }
}
