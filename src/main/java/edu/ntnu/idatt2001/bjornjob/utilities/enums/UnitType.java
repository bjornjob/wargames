package edu.ntnu.idatt2001.bjornjob.utilities.enums;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link UnitType} is an enum class used to represent the subclasses of the superclass {@link Unit}. Each enum is
 * represented by an integer.
 */
public enum UnitType {
    INFANTRY_UNIT(1),
    RANGED_UNIT(2),
    CAVALRY_UNIT(3),
    COMMANDER_UNIT(4);

    private final int code;

    /**
     * A constructor which appoints an integer value to each of the enums.
     * @param code is an integer value (1-4) representing each enum.
     */
    UnitType(int code) {
        this.code = code;
    }

    /**
     * Get method for the code inputted in the constructor.
     * @return the integer value representing each enum.
     */
    public int getCode() {
        return this.code;
    }
}