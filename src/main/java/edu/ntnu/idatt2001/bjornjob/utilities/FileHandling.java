package edu.ntnu.idatt2001.bjornjob.utilities;

import edu.ntnu.idatt2001.bjornjob.simulator.Army;
import edu.ntnu.idatt2001.bjornjob.unitClasses.*;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link FileHandling} is a class used to save and read {@link Army} objects to/from .csv files.
 */
public class FileHandling {
    private static final String DELIMITER = ",";
    private static final String NEWLINE = "\n";

    /**
     * Private constructor for the class {@link FileHandling}. Private in order make in impossible to instantiate any
     * objects of the class.
     */
    public FileHandling() {}

    /**
     * Writes a list of {@link Army} objects to a file.
     * @param army A list of {@link Army} objects.
     * @param file An abstract representation of a file.
     * @throws IOException If an I/O error occurs.
     */
    public void writeArmy(Army army, File file) throws IOException {
        if (!file.getName().endsWith(".csv")) {
            throw new IOException("Unsupported file format. Only .csv-files are supported.");
        }

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.append(army.getName()).append("\n");
            army.getAllUnits().forEach(unit ->
                    {
                        try {
                            fileWriter.append(unit.getClass().getSimpleName()).append(DELIMITER);
                            fileWriter.append(unit.getName()).append(DELIMITER);
                            fileWriter.append(String.valueOf(unit.getHealth())).append(NEWLINE);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
        } catch (IOException e) {
            throw new IOException("Unable to write army file: " + e.getMessage());
        }
    }

    /**
     * Reads a list of {@link Army} objects from a file.
     * @param file is an abstract representation of a file.
     * @throws IOException if an I/O error occurs.
     */
    public Army readArmy(File file) throws IOException {
        if (!file.getName().endsWith(".csv")) {
            throw new IOException("Unsupported file format. Only .csv-files are supported.");
        }

        Army army = null;
        try (Scanner scanner = new Scanner(file)) {
            String armyName = scanner.nextLine();
            army = new Army(armyName);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] tokens = line.split(DELIMITER);

                if (tokens.length != 3) {
                    throw new IOException
                            ("Line data '" + line + "' is invalid. Make sure each line is correctly formatted.");
                }

                String typeOfUnit = tokens[0];
                String nameOfUnit = tokens[1];
                int healthOfUnit;

                try {
                    healthOfUnit = Integer.parseInt(tokens[2]);
                } catch (NumberFormatException e) {
                    throw new IOException("Health value must be an integer (" + e.getMessage() + ").");
                }

                if (healthOfUnit <= 0) {
                    throw new IOException("Health value must be a positive integer.");
                }

                switch (typeOfUnit) {
                    case "InfantryUnit" -> {
                        Unit unit = new InfantryUnit(nameOfUnit, healthOfUnit);
                        army.add(unit);
                    }
                    case "RangedUnit" -> {
                        Unit unit = new RangedUnit(nameOfUnit, healthOfUnit);
                        army.add(unit);
                    }
                    case "CavalryUnit" -> {
                        Unit unit = new CavalryUnit(nameOfUnit, healthOfUnit);
                        army.add(unit);
                    }
                    case "CommanderUnit" -> {
                        Unit unit = new CommanderUnit(nameOfUnit, healthOfUnit);
                        army.add(unit);
                    }
                    default -> throw new IOException("The unit type: " + "'" + typeOfUnit + "' does not exist.");
                }

                /**
                try {
                    Unit unit = UnitFactory.generateUnit(typeOfUnit, nameOfUnit, healthOfUnit);
                }*/
            }
        } catch (IOException e) {
            throw new IOException("Unable to read unit data from the file '" + file.getName() + "': " + e.getMessage());
        }
        return army;
    }

    /**
     * Method to test and correct a file location
     * @param fileLocation the file location, may only be the name of the file or the full location and type
     * @return a sorted and corrected file location, type only using '/'
     */
    /**
    private String testFileLocation(String fileLocation) {
        String fixedFileLocation = fileLocation;
        //Making sure the location ends with .csv
        if (!fixedFileLocation.endsWith(".csv")){
            fixedFileLocation = (fixedFileLocation + ".csv");
        }
        //Making sure the file ends up in the resources folder
        if (!fixedFileLocation.startsWith("src/main/resources/ArmyFiles/" )) {
            fixedFileLocation = ("src/main/resources/ArmyFiles/"  + fixedFileLocation);
        }
        /*
        //Using '/' instead of '\' so that the application can be used on Linux and MacOS
        if(fixedFileLocation.contains("\\")){
            throw new IllegalArgumentException("You used \\ when / was applicable, please change this");
        }
         */
        //return fixedFileLocation;
    //}

    /**
    public Army loadArmy(String fileLocation) throws Exception {
        //Fixing potential errors with the location
        fileLocation = testFileLocation(fileLocation);

        FileInputStream fileInput = new FileInputStream(fileLocation);
        Scanner scanner = new Scanner(fileInput);
        List<Unit> newUnits = new ArrayList<>();

        //Creating army with name
        String name = scanner.nextLine();
        if (name.isBlank()) {
            throw new IllegalArgumentException("The army name is Blank");
        }
        //Army newArmy = new Army(name);

        //Adding saved units
        while (scanner.hasNextLine()) {
            String thisLine = scanner.nextLine();
            String[] lineVariables = thisLine.split(",");
            if (lineVariables.length > 1 && !thisLine.isBlank()) {
                // Making the unit
                switch (lineVariables[0].trim()) {
                    case "InfantryUnit" -> {
                        InfantryUnit unit = new InfantryUnit(lineVariables[1], Integer.parseInt(lineVariables[2]));
                        newUnits.add(unit);
                    }
                    case "RangedUnit" -> {
                        RangedUnit unit = new RangedUnit(lineVariables[1], Integer.parseInt(lineVariables[2]));
                        newUnits.add(unit);
                    }
                    case "CavalryUnit" -> {
                        CavalryUnit unit = new CavalryUnit(lineVariables[1], Integer.parseInt(lineVariables[2]));
                        newUnits.add(unit);
                    }
                    case "CommanderUnit" -> {
                        CommanderUnit unit = new CommanderUnit(lineVariables[1], Integer.parseInt(lineVariables[2]));
                        newUnits.add(unit);
                    }
                    default -> throw new IllegalArgumentException("Unit must have a valid name");
                }
            }
        }
        //Closing file and changing units in the Army file
        scanner.close();
        //newArmy.setUnits(newUnits);

        // Returning Army
        return new Army(fileLocation, newUnits);
    }*/
}