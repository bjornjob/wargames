package edu.ntnu.idatt2001.bjornjob.simulator;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.TerrainType;

import java.util.Random;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link Battle} represents a battle between two armies of the class {@link Army}.
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private TerrainType terrainType;

    /**
     * Creates a new instance of a {@link Battle}.
     * @param armyOne is the first army which will partake in the battle.
     * @param armyTwo is the second army which will partake in the battle.
     * @throws IllegalArgumentException if one or both inputted armies are empty.
     */
    public Battle(Army armyOne, Army armyTwo, TerrainType terrainType) throws IllegalArgumentException {
        if(!armyOne.hasUnits() || !armyTwo.hasUnits()) {
            throw new IllegalArgumentException("Inputted army must contain soldiers!");
        }
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrainType = terrainType;
    }

    /**
     * A method simulating a battle between the two armies initiated in the class constructor.
     * @return an object of the winning army.
     */
    public Army simulate() {
        while(battleIsActive()) {
           Unit unitFromArmyOne = armyOne.getRandom();
           Unit unitFromArmyTwo = armyTwo.getRandom();
           generateRandomAttackSequence(unitFromArmyOne, unitFromArmyTwo);
       }

       if(armyOne.hasUnits()) {
           return armyOne;
       } else {
           return armyTwo;
       }
    }

    /**
     * Generates a random integer which will be used in the {@link #generateRandomAttackSequence(Unit, Unit)} method in
     * order to make the simulation of a battle between two armies realistic.
     * @return a random integer - either 0 or 1.
     */
    private int randomIntegerGenerator() {
        Random randomInteger = new Random();
        return randomInteger.nextInt(2);
    }

    /**
     * A method for checking if both armies have living units.
     * @return true if both armies has living units, false if not.
     */
    public boolean battleIsActive() {
        return armyOne.hasUnits() && armyTwo.hasUnits();
    }

    /**
     * A method for checking if a unit is dead or alive.
     * @param defendingUnit the unit which is checked.
     * @return true if the unit is dead, false if not.
     */
    public boolean checkIfUnitIsDead(Unit defendingUnit) {
        return defendingUnit.getHealth() == 0;
    }

    /**
     * A method which, based on a random generated integer between 0 and 1, chooses whom of the two units from each
     * army in the battle attacks first and is the recipe of how the battle is fought.
     *
     * The method checks if the unit which is being attacked first is dead or alive. If it dies, it is removed
     * from its army. If it survives, it makes a counterattack.
     * Finally the method checks if the initial attacking unit is dead or alive. If it dies, it is removed from its
     * army.
     *
     * @param unitFromArmyOne is a randomly chosen unit from the first army.
     * @param unitFromArmyTwo is a randomly chosen unit from the second army.
     */
    public void generateRandomAttackSequence(Unit unitFromArmyOne, Unit unitFromArmyTwo) {
        if(randomIntegerGenerator() == 0 && armyOne.hasUnits()) {
            unitFromArmyOne.attack(unitFromArmyTwo, terrainType.getCode());
            if(checkIfUnitIsDead(unitFromArmyTwo)) {
                armyTwo.remove(unitFromArmyTwo);
            }
            if(battleIsActive()) {
                unitFromArmyTwo.attack(unitFromArmyOne, terrainType.getCode());
            }
            if(checkIfUnitIsDead(unitFromArmyOne)) {
                armyOne.remove(unitFromArmyOne);
            }
        } else if(randomIntegerGenerator() == 1 && armyTwo.hasUnits()) {
            unitFromArmyTwo.attack(unitFromArmyOne, terrainType.getCode());
            if(checkIfUnitIsDead(unitFromArmyOne)) {
                armyOne.remove(unitFromArmyOne);
            }
            if(battleIsActive()) {
                unitFromArmyOne.attack(unitFromArmyTwo, terrainType.getCode());
            }
            if(checkIfUnitIsDead(unitFromArmyTwo)) {
                armyTwo.remove(unitFromArmyTwo);
            }
        }
    }

    /**
     * Get string version of a Battle object.
     * @return a string representing an object of the class {@link Battle}.
     */
    @Override
    public String toString() {
        return "\nBattle" +
                "\narmyOne: " + armyOne +
                "\narmyTwo: " + armyTwo;
    }
}
