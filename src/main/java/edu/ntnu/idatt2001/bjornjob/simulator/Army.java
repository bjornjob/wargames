package edu.ntnu.idatt2001.bjornjob.simulator;

import edu.ntnu.idatt2001.bjornjob.unitClasses.*;
import edu.ntnu.idatt2001.bjornjob.unitClasses.specialized.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link Army} represents an army holding a name, as well as a list of the different subclasses of the class
 * {@link Unit} the army consists of.
 */
public class Army {
    private String name;
    private List<Unit> units;

    /**
     * Creates a new instance of a {@link Army}.
     * @param name is the name of the army.
     * @throws IllegalArgumentException if inputted name is null/blank.
     */
    public Army(String name) throws IllegalArgumentException, NullPointerException {
        checkNameInputForNullAndBlank(name);
        this.name = name.trim();
        this.units = new ArrayList<>();
    }

    /**
     * Creates a new instance of a {@link Army}.
     * @param name is the name of the army.
     * @param units is a list consisting of different subclasses of the class {@link Unit}.
     * @throws IllegalArgumentException if inputted name is null/blank or if the inputted list is not an ArrayList.
     */
    public Army(String name, List<Unit> units) throws IllegalArgumentException, NullPointerException {
        checkNameInputForNullAndBlank(name);
        checkIfInputListIsValid(units);
        this.name = name.trim();
        this.units = units;
    }

    /**
     * Gets the name of the unit.
     * @return the name of the unit.
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a single unit to the army.
     * @param unit is an object of type {@link Unit}.
     */
    public void add(Unit unit) {
        this.units.add(unit);
    }

    /**
     * Appends a list of units to the army.
     * @param units is objects of type {@link Unit}.
     * @throws IllegalArgumentException if the inputted list is not an ArrayList.
     */
    public void addAll(List<Unit> units) throws IllegalArgumentException {
        checkIfInputListIsValid(units);
        this.units.addAll(units);
    }

    /**
     * Removes a single unit from the army.
     * @param unit is an object of type {@link Unit}.
     * @throws IllegalArgumentException if the army don´t contains the inputted unit.
     */
    public void remove(Unit unit) throws IllegalArgumentException {
        if(!units.contains(unit)) {
            throw new IllegalArgumentException("The unit does not exist!");
        } else {
            this.units.remove(unit);
        }
    }

    /**
     * Checks if the army contains any units.
     * @return false if the army is empty, true if the army contains one or more units.
     */
    public boolean hasUnits() {
        return units.size() != 0;
    }

    /**
     * Gets all units in the army.
     * @return a list consisting of all the units.
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Generates a random integer based on the length of a list of units with an upper bound (upper value included)
     * which is the length of the list. This integer is used in the {@link #getRandom()} method.
     * @param units is the list of units which decides the upper bound value.
     * @return a random integer.
     */
    private int randomIntegerGenerator(List<Unit> units) {
        Random randomInteger = new Random();
        return randomInteger.nextInt(units.size());
    }

    /**
     * Takes a random integer from the {@link #randomIntegerGenerator(List)} method and uses this in order to pick a
     * random unit from the army.
     * @return a random unit from the army.
     */
    public Unit getRandom() {
        return units.get(randomIntegerGenerator(units));
     }

    /**
     * A method which finds and collects all instances of the type {@link InfantryUnit} from the army, to a list.
     * @return a list consisting of all the collected objects.
     */
    public List<Unit> getInfantryUnits() {
        return units.stream()
                .filter(unit -> unit instanceof InfantryUnit)
                .collect(Collectors.toList());
     }

    /**
     * A method which finds and collects all instances of the type {@link CavalryUnit} from the army, to a list.
     * @return a list consisting of all the collected objects.
     */
    public List<Unit> getCavalryUnits() {
        return units.stream()
                .filter(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit))
                .collect(Collectors.toList());
    }

    /**
     * A method which finds and collects all instances of the type {@link RangedUnit} from the army, to a list.
     * @return a list consisting of all the collected objects.
     */
    public List<Unit> getRangedUnits() {
        return units.stream()
                .filter(unit -> unit instanceof RangedUnit)
                .collect(Collectors.toList());
    }

    /**
     * A method which finds and collects all instances of the type {@link CommanderUnit} from the army, to a list.
     * @return a list consisting of all the collected objects.
     */
    public List<Unit> getCommanderUnits() {
        return units.stream()
                .filter(unit -> unit instanceof CommanderUnit)
                .collect(Collectors.toList());
    }

    /**
     * A method which throws an IllegalArgumentException if inputted name is null or blank.
     * @param name the name of an army.
     */
    private void checkNameInputForNullAndBlank(String name) throws IllegalArgumentException, NullPointerException {
        if (name == null) {
            throw new NullPointerException("The name for an army cannot be null, please try again!");
        } else if (name.isBlank()) {
            throw new IllegalArgumentException("The name for an army cannot be blank, please try again!");
        }
    }

    /**
     * A method which throws an IllegalArgumentException if inputted list is not an ArrayList.
     * @param units is a list of units to bed added to the army.
     */
    private void checkIfInputListIsValid(List<Unit> units) throws IllegalArgumentException {
        if(!(units instanceof ArrayList)) {
            throw new IllegalArgumentException("The list-type must be an arraylist, please try again!");
        }
    }

    /**
     * Get string version of an army object.
     * @return a string representing an object of the class {@link Army}.
     */
    @Override
    public String toString() {
        return "\nName of army:" + name +
                "\nlist of units:" + units;
    }

    /**
     * Checks if two {@link Army} objects are equal.
     * @param o inputted object.
     * @return false if the objects aren´t equal, true if they are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * Gets the hashcode of the object.
     * @return the objects hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}