package edu.ntnu.idatt2001.bjornjob.guiControllers;

import edu.ntnu.idatt2001.bjornjob.utilities.Admin;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class ArmyOneStatsController implements Initializable {

    @FXML
    private TextArea RemainingUnitsInArmyOne;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadRemainingUnits();
    }

    @FXML
    public void loadRemainingUnits() {
        if(Admin.getArmyOne() == null) {
            RemainingUnitsInArmyOne.setText("There are no units in this army!");
        } else if (!Admin.getArmyOne().hasUnits()) {
            RemainingUnitsInArmyOne.setText("There are no units in this army!");
        } else {
            RemainingUnitsInArmyOne.setText(Admin.getArmyOne().toString());
        }
    }
}
