package edu.ntnu.idatt2001.bjornjob.guiControllers;

import edu.ntnu.idatt2001.bjornjob.simulator.Army;
import edu.ntnu.idatt2001.bjornjob.utilities.Admin;
import edu.ntnu.idatt2001.bjornjob.utilities.FileHandling;
import edu.ntnu.idatt2001.bjornjob.utilities.enums.TerrainType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    FileHandling fileHandling = new FileHandling();

    @FXML
    private Label nameOfHumanArmy;

    @FXML
    private Label sizeOfHumanArmy;

    @FXML
    private Label numberOfHumanInfantryUnits;

    @FXML
    private Label numberOfHumanRangedUnits;

    @FXML
    private Label numberOfHumanCavalryUnits;

    @FXML
    private Label numberOfHumanCommanderUnits;

    @FXML
    private Label nameOfOrcishHorde;

    @FXML
    private Label sizeOfOrcishHorde;

    @FXML
    private Label numberOfOrcishInfantryUnits;

    @FXML
    private Label numberOfOrcishRangedUnits;

    @FXML
    private Label numberOfOrcishCavalryUnits;

    @FXML
    private Label numberOfOrcishCommanderUnits;

    @FXML
    private Label showWinner;

    @FXML
    private Label showArmyOneFilePath;

    @FXML
    private Label showArmyTwoFilePath;

    @FXML
    private Label exceptionMessage;

    @FXML
    private ChoiceBox<String> choiceBox;

    /**
     * Initializes the choicebox with items for choosing a terrain.
     * The value is by default set to 'Hill'.
     */
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        choiceBox.getItems().addAll("Hill", "Plains", "Forest");
        choiceBox.setValue("Hill");
    }

    /**
     * A method which first runs the {@link #readArmyOne()} method before setting the values of the army by running the
     * {@link #setValuesForArmyOne()} method. This is used by the 'Load army' button in the GUI in order to load the army to the
     * GUI.
     */
    @FXML
    protected void loadArmyOne() {
        try {
            readArmyOne();
            setValuesForArmyOne();
        } catch (IOException | NullPointerException | NumberFormatException e) {
            System.out.println((e.getMessage()));
            exceptionMessage.setText("For army one: " + e.getMessage());
        }
    }

    /**
     * A method which first runs the {@link #readArmyTwo()} method before setting the values of the army by running the
     * {@link #setValuesForArmyTwo()} method. This is used by the 'Load army' button in the GUI in order to load the army to the
     * GUI.
     */
    @FXML
    protected void loadArmyTwo() {
        try {
            readArmyTwo();
            setValuesForArmyTwo();
        } catch (IOException | NullPointerException | NumberFormatException e) {
            System.out.println((e.getMessage()));
            exceptionMessage.setText("For army two: " + e.getMessage());
        }
    }

    /**
     * A method for reading an army from a .csv file. The file is read with {@link FileHandling} and saved in
     * {@link Admin}. The method also reads and sets the path of where the file is read from.
     * @throws IOException in any case a file is considered invalid, see {@link FileHandling} for details.
     */
    private void readArmyOne() throws IOException {
        Admin.setArmyOne(fileHandling.readArmy
                (new File("src/main/resources/edu.ntnu.idatt2001.bjornjob/.csv files/HumanArmy.csv")));
        File file = new File("src/main/resources/edu.ntnu.idatt2001.bjornjob/.csv files/HumanArmy.csv");
        showArmyOneFilePath.setText("Army was loaded from: " + file.getPath());
    }

    /**
     * A method for reading an army from a .csv file. The file is read with {@link FileHandling} and saved in
     * {@link Admin}. The method also reads and sets the path of where the file is read from.
     * @throws IOException in any case a file is considered invalid, see {@link FileHandling} for details.
     */
    private void readArmyTwo() throws IOException {
        Admin.setArmyTwo(fileHandling.readArmy
                (new File("src/main/resources/edu.ntnu.idatt2001.bjornjob/.csv files/OrcishHorde.csv")));
        File file = new File("src/main/resources/edu.ntnu.idatt2001.bjornjob/.csv files/OrcishHorde.csv");
        showArmyTwoFilePath.setText("Army was loaded from: " + file.getPath());
    }

    /**
    private Army getArmyFromFileChosen() throws Exception {
        fileHandling = new FileHandling();
        FileChooser fileChooser = new FileChooser();
        File chosenFile = fileChooser.showOpenDialog(null);
        String path = chosenFile.getName();
        return fileHandling.loadArmy(path);
    }*/

    /**
     * A method for resetting army one in the GUI.
     */
    @FXML
    protected void resetHumanArmy() {
        loadArmyOne();
        exceptionMessage.setText("");
    }

    /**
     * A method for resetting army one in the GUI.
     */
    @FXML
    protected void resetOrcishHorde() {
        loadArmyTwo();
        exceptionMessage.setText("");
    }

    @FXML
    public void viewArmyOneUnits() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("army-one-stats.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Army one units");
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    @FXML
    public void viewArmyTwoUnits() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("army-two-stats.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Army two units");
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    /**
     * A method for setting the terrain of the battle based on user input through the choicebox in the GUI.
     * @return an enum of {@link TerrainType}.
     */
    private TerrainType setTerrain() {
        switch (choiceBox.getValue()) {
            default -> {
                return TerrainType.HILL;
            }
            case "Plains" -> {
                return TerrainType.PLAINS;
            }
            case "Forest" -> {
                return TerrainType.FOREST;
            }
        }
    }

    /**
     *
     */
    @FXML
    public void simulateBattle() {
        try {
            Admin.setBattle(setTerrain());
            String winner = Admin.getBattle().simulate().getName();
            showWinner.setText(winner);
            setValuesForArmyOne();
            setValuesForArmyTwo();
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println((e.getMessage()));
            exceptionMessage.setText(e.getMessage());
        }
    }

    /**
     * A method which sets the values for army one. These values are read from {@link Admin}.
     */
    private void setValuesForArmyOne() {
        nameOfHumanArmy.setText(Admin.getArmyOne().getName());
        sizeOfHumanArmy.setText(String.valueOf(Admin.getArmyOne().getAllUnits().size()));
        numberOfHumanInfantryUnits.setText(String.valueOf(Admin.getArmyOne().getInfantryUnits().size()));
        numberOfHumanRangedUnits.setText(String.valueOf(Admin.getArmyOne().getRangedUnits().size()));
        numberOfHumanCavalryUnits.setText(String.valueOf(Admin.getArmyOne().getCavalryUnits().size()));
        numberOfHumanCommanderUnits.setText(String.valueOf(Admin.getArmyOne().getCommanderUnits().size()));
    }

    /**
     * A method which sets the values for army two. These values are read from {@link Admin}.
     */
    private void setValuesForArmyTwo() {
        nameOfOrcishHorde.setText(Admin.getArmyTwo().getName());
        sizeOfOrcishHorde.setText(String.valueOf(Admin.getArmyTwo().getAllUnits().size()));
        numberOfOrcishInfantryUnits.setText(String.valueOf(Admin.getArmyTwo().getInfantryUnits().size()));
        numberOfOrcishRangedUnits.setText(String.valueOf(Admin.getArmyTwo().getRangedUnits().size()));
        numberOfOrcishCavalryUnits.setText(String.valueOf(Admin.getArmyTwo().getCavalryUnits().size()));
        numberOfOrcishCommanderUnits.setText(String.valueOf(Admin.getArmyTwo().getCommanderUnits().size()));
    }
}