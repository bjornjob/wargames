package edu.ntnu.idatt2001.bjornjob.guiControllers;

import edu.ntnu.idatt2001.bjornjob.utilities.Admin;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class ArmyTwoStatsController implements Initializable {

    @FXML
    private TextArea RemainingUnitsInArmyTwo;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadRemainingUnits();
    }

    @FXML
    public void loadRemainingUnits() {
        if(Admin.getArmyTwo() == null) {
            RemainingUnitsInArmyTwo.setText("There are no units in this army!");
        } else if (!Admin.getArmyTwo().hasUnits()) {
            RemainingUnitsInArmyTwo.setText("There are no units in this army!");
        } else {
            RemainingUnitsInArmyTwo.setText(Admin.getArmyTwo().toString());
        }
    }
}
