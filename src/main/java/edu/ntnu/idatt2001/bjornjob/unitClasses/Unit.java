package edu.ntnu.idatt2001.bjornjob.unitClasses;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * {@link Unit} represents a solider holding a name, as well as values for health, attack and armor.
 */
public abstract class Unit {
    private final String name;
    private int health;
    private final int attack;
    private final int armor;

    /**
     * Creates a new instance of a {@link Unit}.
     * NOTE: The constructor has been made protected instead of public.
     * Since this is an abstract class, it should not be possible to
     * create a new instance of the class directly. Hence the only use of
     * the constructor will be by subclasses. Subclasses have access to protected members.
     *
     * @param name is the name of the unit.
     * @param health is the health of the unit.
     * @param attack is the damage the unit does to an opponent.
     * @param armor is how much the unit can withstand in a attack.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health, attack or armor
     *                                  is negative.
     */
    protected Unit(String name, int health, int attack, int armor) throws IllegalArgumentException,
            NullPointerException {
        checkNameInputForNullAndBlank(name);
        checkHealthInputForNegativeValue(health);
        checkAttackInputForNegativeValue(attack);
        checkArmorInputForNegativeValue(armor);
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Calculates the damage one unit does to another unit in battle. The health of the opponent which is attacked
     * is set using the `setHealth` method in this class.
     * @param opponent is the opponent whom is attacked.
     */
    public void attack(Unit opponent, int terrainType) {
        int opponentHealthAfterAttack = opponent.getHealth() - (this.getAttack() + this.getAttackBonus(terrainType)) +
                                        (opponent.getArmor() + opponent.getResistBonus(terrainType));
        opponent.setHealth(opponentHealthAfterAttack);
    }

    /**
     * Gets the name of the unit.
     * @return the name of the unit.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the health of the unit.
     * @return the health of the unit.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets the attack-value of the unit.
     * @return the attack-value of the unit.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets the armor-value of the unit.
     * @return the armor-value of the unit.
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets the health of the unit. A unit is considered dead when its health is zero.
     * If a negative value is inputted, the health is set to zero.
     * @param health is the health value of a unit.
     */
    public void setHealth(int health) {
        this.health = Math.max(health, 0);
    }

    /**
     * An abstract method used to calculate different attack bonuses for each subclass of {@link Unit}.
     * @return the attack bonus of the subclass-unit.
     */
    public abstract int getAttackBonus(int terrainType);

    /**
     * An abstract method used to calculate different resist (armor) bonuses for each subclass of {@link Unit}.
     * @return the resist bonus of the subclass-unit.
     */
    public abstract int getResistBonus(int terrainType);

    /**
     * A method which throws an IllegalArgumentException if inputted name is null or blank.
     * @param name the name of a unit.
     */
    private void checkNameInputForNullAndBlank(String name) throws IllegalArgumentException, NullPointerException {
        if (name == null) {
            throw new NullPointerException("Name of the unit cannot be null!");
        } else if (name.isBlank()) {
                throw new IllegalArgumentException("Name of the unit cannot be blank!");
            }
    }

    /**
     * A method which throws an IllegalArgumentException if inputted health is negative.
     * @param healthInput the name of a unit.
     */
    private void checkHealthInputForNegativeValue(int healthInput) throws IllegalArgumentException {
        if (healthInput <= 0) {
            throw new IllegalArgumentException("Health of the unit cannot be negative or zero!");
        }
    }

    /**
     * A method which throws an IllegalArgumentException if inputted attack-value of a unit is negative.
     * @param attackInput the name of a unit.
     */
    private void checkAttackInputForNegativeValue(int attackInput) throws IllegalArgumentException {
        if (attackInput < 0) {
            throw new IllegalArgumentException("Attack for the unit cannot be negative!");
        }
    }

    /**
     * A method which throws an IllegalArgumentException if inputted armor-value of a unit is negative.
     * @param armorInput the name of a unit.
     */
    private void checkArmorInputForNegativeValue(int armorInput) throws IllegalArgumentException {
        if (armorInput < 0) {
            throw new IllegalArgumentException("Armor for the unit cannot be negative!");
        }
    }

    /**
     * Get string version of a unit object.
     * @return a string representing an object of the class {@link Unit}.
     */
    @Override
    public String toString() {
        return "\nName of unit: " + name +
                "\nType of unit: " + this.getClass().getSimpleName() +
                "\nhealth: " + health +
                "\nattack: " + attack +
                "\narmor: " + armor;
    }
}
