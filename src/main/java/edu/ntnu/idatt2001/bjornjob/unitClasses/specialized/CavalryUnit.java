package edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * A CavalryUnit is a {@link Unit} which represents a specific type of solider.
 */
public class CavalryUnit extends Unit {
    private int attackNumber = 0;

    /**
     * Creates a new instance of a {@link CavalryUnit}.
     * @param name is the name of the CavalryUnit.
     * @param health is the health of the CavalryUnit.
     * @param attack is the damage the CavalryUnit does to an opponent.
     * @param armor is how much the CavalryUnit can withstand in an attack.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health, attack or armor
     *                                  is negative.
     */
    public CavalryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException,
            NullPointerException {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new instance of a {@link CavalryUnit}. This constructor Uses predefined values for attack and armor.
     * @param name is the name of the CavalryUnit.
     * @param health is the health of the CavalryUnit.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public CavalryUnit(String name, int health) throws IllegalArgumentException, NullPointerException {
        super(name, health, 20, 12);
    }

    /**
     * Gets the number of times the unit has made an attack.
     * @return the number of attacks.
     */
    public int getAttackNumber() {
        return attackNumber;
    }

    /**
     * Sets the number of times the unit has made an attack.
     * @param attackNumber is the number of attacks.
     */
    private void setAttackNumber(int attackNumber) {
        this.attackNumber = attackNumber;
    }

    /**
     * {@link CavalryUnit} has a bigger advantage when attacking an opponent for the first time (charge), than in melee.
     * @return the attack bonus of the unit.
     */
    @Override
    public int getAttackBonus(int terrainType) {
        int charge = 6;
        int melee = 2;
        int bonus = 0;

        if (terrainType == 2) {
            bonus += 2;
        }

        if(getAttackNumber() == 0) {
            setAttackNumber(1);
            return charge + bonus;
        } else
            setAttackNumber(getAttackNumber() + 1);
        return melee + bonus;
    }

    /**
     * {@link CavalryUnit} has a small defense capability bonus in melee.
     * @return the resist bonus of the unit.
     */
    @Override
    public int getResistBonus(int terrainType) {
        if (terrainType == 3) {
            return 0;
        } else {
            return 1;
        }
    }
}