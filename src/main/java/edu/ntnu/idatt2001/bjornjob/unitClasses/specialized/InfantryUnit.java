package edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * A InfantryUnit is a {@link Unit} which represents a specific type of solider.
 */
public class InfantryUnit extends Unit {

    /**
     * Creates a new instance of a {@link InfantryUnit}.
     * @param name is the name of the InfantryUnit.
     * @param health is the health of the InfantryUnit.
     * @param attack is the damage the InfantryUnit does to an opponent.
     * @param armor is how much the InfantryUnit can withstand in a attack.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health, attack or armor
     *                                  is negative.
     */
    public InfantryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException,
            NullPointerException {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new instance of a {@link InfantryUnit}. This constructor Uses predefined values for attack and armor.
     * @param name is the name of the InfantryUnit.
     * @param health is the health of the InfantryUnit.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public InfantryUnit(String name, int health) throws IllegalArgumentException, NullPointerException {
        super(name, health, 15, 10);
    }

    /**
     * {@link InfantryUnit} has a strength bonus in melee.
     * @return the attack bonus of the unit.
     */
    @Override
    public int getAttackBonus(int terrainType) {
        if (terrainType == 3) {
            return 2 + 2;
        } else {
            return 2;
        }
    }

    /**
     * {@link InfantryUnit} has a small defense capability bonus in melee.
     * @return the resist bonus of the unit.
     */
    @Override
    public int getResistBonus(int terrainType) {
        if (terrainType == 3) {
            return 1 + 2;
        } else {
            return 1;
        }
    }
}
