package edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * The CommanderUnit inherit from the {@link CavalryUnit} class.
 * A CommanderUnit is a {@link Unit} which represents a Commander of a {@link CavalryUnit}.
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Creates a new instance of a {@link CommanderUnit}.
     * @param name is the name of the CommanderUnit.
     * @param health is the health of the CommanderUnit.
     * @param attack is the damage the CommanderUnit does to an opponent.
     * @param armor is how much the CommanderUnit can withstand in a attack.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health, attack or armor
     *                                  is negative.
     */
    public CommanderUnit(String name, int health, int attack, int armor) throws IllegalArgumentException,
            NullPointerException {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new instance of a {@link CommanderUnit}. This constructor Uses predefined values for attack and armor.
     * @param name is the name of the CommanderUnit.
     * @param health is the health of the CommanderUnit.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public CommanderUnit(String name, int health) throws IllegalArgumentException, NullPointerException {
        super(name, health, 25, 15);
    }
}