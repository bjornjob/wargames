package edu.ntnu.idatt2001.bjornjob.unitClasses.specialized;

import edu.ntnu.idatt2001.bjornjob.unitClasses.Unit;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 *
 * A RangedUnit is a {@link Unit} which represents a specific type of solider.
 */
public class RangedUnit extends Unit {
    private int attackNumber = 0;

    /**
     * Creates a new instance of a {@link RangedUnit}.
     * @param name is the name of the RangedUnit.
     * @param health is the health of the RangedUnit.
     * @param attack is the damage the RangedUnit does to an opponent.
     * @param armor is how much the RangedUnit can withstand in an attack.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health, attack or armor
     *                                  is negative.
     */
    public RangedUnit(String name, int health, int attack, int armor) throws IllegalArgumentException,
            NullPointerException {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new instance of a {@link RangedUnit}. This constructor Uses predefined values for attack and armor.
     * @param name is the name of the RangedUnit.
     * @param health is the health of the RangedUnit.
     * @throws IllegalArgumentException if inputted name is null/blank or if inputted value for health is negative.
     */
    public RangedUnit(String name, int health) throws IllegalArgumentException, NullPointerException {
        super(name, health, 15, 8);
    }

    /**
     * Gets the number of times the unit has made an attack.
     * @return the number of attacks.
     */
    public int getAttackNumber() {
        return attackNumber;
    }

    /**
     * Sets the number of times the unit has made an attack.
     * @param attackNumber is the number of attacks.
     */
    private void setAttackNumber(int attackNumber) {
        this.attackNumber = attackNumber;
    }

    /**
     * {@link RangedUnit} has an advantage due to being able to attack from a distance.
     * @return the attack bonus of the unit.
     */
    @Override
    public int getAttackBonus(int terrainType) {
        if (terrainType == 1) {
            return 3 + 2;
        } else if (terrainType == 3) {
            return 3-1;
        } else {
            return 3;
        }
    }

    /**
     * {@link RangedUnit} has a defence capability bonus which is based on it´s distance from the enemy.
     * @return the resist bonus of the unit.
     */
    @Override
    public int getResistBonus(int terrainType) {
        int firstAttack = 6;
        int secondAttack = 4;
        int furtherAttacks = 2;

        if(getAttackNumber() == 0) {
            setAttackNumber(1);
            return firstAttack;
        } else if (getAttackNumber() == 1) {
            setAttackNumber(getAttackNumber() + 1);
            return secondAttack;
        } else
            setAttackNumber(getAttackNumber() + 1);
        return furtherAttacks;
    }
}