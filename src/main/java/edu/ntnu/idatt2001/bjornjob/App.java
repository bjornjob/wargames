package edu.ntnu.idatt2001.bjornjob;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author bjornjob
 * @version 1.o
 * @since 20.02.2022
 */
public class App extends javafx.application.Application {

    @Override
    public void start(Stage stage) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class
                    .getResource("/edu.ntnu.idatt2001.bjornjob/fxml/main-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Wargames");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}